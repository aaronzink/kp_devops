# Bluecat API integration for devops
# Version 0.1
# azink@trace3.com

require 'savon'
require 'ipaddress'

module Bluecat
  class Api
    attr_accessor :client
    attr_accessor :auth_cookies

    def initialize(host, username, password)
      # Connect to Bluecat SOAP Api and login
      url = "https://#{host}/Services/API?wsdl"
      @client = Savon.client(wsdl: url, ssl_verify_mode: :none)
      unless client.nil?
        response = client.call(:login) do |c|
          c.message username: username, password: password
        end
        @auth_cookies = response.http.cookies
        response.body
      else
        return nil
      end
    end

    def add_device_instance(hostname, ip_address, zone_name, options)
      # convert offset to an IP and merge
      options[:options] = "offset=#{IPAddress.parse(ip_address).to_a[options[:options][:offset]].address}"
      options.merge!({recordName: hostname, ipEntity: ip_address, zoneName: zone_name})
      
      # Make API call, catch and report any SOAP errors
      begin
        response = client.call(:add_device_instance) do |c|
          c.cookies auth_cookies
          c.message options
        end
        response.body
      rescue Savon::SOAPFault => e
        e.message
      end
    end

    def delete_device_instance(ip_address, options)
      # remove unneeded defaults
      options = { config_name: options[:config_name], identifier: ip_address, options: nil }
      
      # Make API call, catch and report any SOAP errors
      begin
        response = client.call(:delete_device_instance) do |c|
          c.cookies auth_cookies
          c.message options
        end
        response.body
      rescue Savon::SOAPFault => e
        e.message
      end
    end    
  end
end