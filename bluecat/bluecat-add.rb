#!/usr/bin/env ruby

require 'optparse'
require_relative 'defaults'
require_relative 'lib/bluecat'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Add records to Bluecat\nUsage: bluecat-add.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required Bluecat arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Bluecat Proteus") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-r", "--record_name RECORD_NAME", "Name of host") {|x| args[:record_name] = x }
  opts.on("-i", "--ip_address IP/CIDR", "IP address and subnet") {|x| args[:ip_address] = x }
  opts.on("-z", "--zone ZONE", "Zone name") {|x| args[:zone] = x }
end.parse!

bc = Bluecat::Api.new(args[:hostname], args[:username], args[:password])
puts bc.add_device_instance(args[:record_name], args[:ip_address], args[:zone], @defaults)
