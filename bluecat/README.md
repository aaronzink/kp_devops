# BlueCat DNS library for DevOps

## Installation (with ruby)
1. Install required gems:
  `bundle install`

## Installation (without ruby)
1. Install chef-client:
  `curl -L https://www.chef.io/chef/install.sh | sudo bash`
2. Extend path (add to .bashrc for permanance):
  `export PATH=$PATH:/opt/chef/embedded/bin`
3. Install required gems:
  `bundle install`

## Usage

### Add a DNS record

Returns information about the added record (including IP) or an error.
```
% ruby ./bluecat-add.rb -h
Add records to Bluecat
Usage: bluecat-add.rb [options]
    -h, --help                       Displays this help
Required Bluecat arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Bluecat Proteus
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -r, --record_name RECORD_NAME    Name of host
    -i, --ip_address IP/CIDR         IP address and subnet
    -z, --zone ZONE                  Zone name
```

### Remove an existing DNS record.

Returns success or an error
```
% ruby ./bluecat-remove.rb -h
Remove records from Bluecat
Usage: bluecat-remove.rb [options]
    -h, --help                       Displays this help
Required Bluecat arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Bluecat Proteus
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -i, --ip_address IP              IP address to remove
```