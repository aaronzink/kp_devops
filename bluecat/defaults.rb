# KP default options
@defaults = {
  config_name: 'KP',
  ip_address_mode: 'REQUEST_STATIC',
  view_name: 'default',
  device_name: nil,
  mac_address_mode: nil,
  mac_entity: nil,
  options: {
    offset: 21, # IPs to offset, will be converted to an IP address
  }
}
