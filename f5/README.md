# Background

ScriptMaster F5 ruby libraries contain a library and a set of scripts to create and manage F5 pools using the iControl API.  The scripts and library are both extensible to meet the requirements of the application, and contains functions specific to DevOps and not the entire iControl library.  See the iControl reference at https://devcentral.f5.com/wiki/iControl.APIReference.ashx to add additional functions.

# Prerequisites
1. Install chef-client:
  `curl -L https://www.chef.io/chef/install.sh | sudo bash`
2. Install required gems:
  `/opt/chef/embedded/bin/gem install f5-icontrol`

# F5 library usage

## Pool functions

### Create a new pool with optional monitor, load balancing method, and members
```
% f5-create-pool.rb -h
Create pool and optionally assign members
Usage: f5-create-pool.rb [options]
    -h, --help                       Displays this help
Required F5 and pool arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool (will be created if it doesn't exist)
Other optional arguments:
    -l, --lb_method [LB_METHOD]      Load balancing method (defaults to LB_METHOD_ROUND_ROBIN)
    -o, --monitor [MONITOR]          Set pool monitor
    -m [SERVER1:PORT,SERVER2:PORT,...],
        --members                    Pool members, comma-separated
```

### Assign pool to an existing VIP
```
% f5-assign-pool.rb -h
Assign a default pool to an existing Virtual Server or new empty VS matching a regex
Usage: f5-assign-pool.rb [options]
    -h, --help                       Displays this help
Required F5 arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool
    -r, --allowed_vs_regex [REGEX]   Allowed Virtual Server match regex, defaults to any
```

### Unassign pool
```
% f5-unassign-pool.rb -h
Unassign the default pool from an existing Virtual Server
Usage: f5-assign-pool.rb [options]
    -h, --help                       Displays this help
Required F5 arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool
```

### Remove pool
```
% f5-remove-pool.rb -h
Delete pool
Usage: f5-delete-pool.rb [options]
    -h, --help                       Displays this help
Required F5 and pool arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool (will be created if it doesn't exist)
```

## Member functions

### Add members to an existing pool
```
% f5-add-members.rb -h
Add members to an existing pool
Usage: f5-add-members.rb [options]
    -h, --help                       Displays this help
Required F5 arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool
    -m SERVER1:PORT,SERVER2:PORT,...,
        --members                    Pool members, comma-separated
```

### Remove members to an existing pool
```
% f5-remove-members.rb -h
Remove members from a pool
Usage: f5-remove-members.rb [options]
    -h, --help                       Displays this help
Required F5 arguments:
    -b, --hostname HOSTNAME          Hostname or IP of management interface of Big-IP
    -u, --username USERNAME          Username for Big-IP
    -p, --password PASSWORD          Password for Big-IP
Script-specific required arguments:
    -n, --pool_name POOL_NAME        Name of pool
    -m SERVER1:PORT,SERVER2:PORT,...,
        --members                    Pool members, comma-separated
```