#!/opt/chef/embedded/bin/ruby

# F5 iControl for KP DevOps - Create pool if pool does not already exist and assigns a monitor
# Expects: Command-line arguments
# Returns:
#  Success: Exit code 0 with string of pool name
#  Success (already exists): Exit code 0, no string

# Aaron Zink - azink@trace3.com
# Version 1.02

require 'rubygems'
require 'optparse'
require './lib/f5-kp-library'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Create pool and optionally assign members\nUsage: f5-create-pool.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required F5 and pool arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Big-IP") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-n", "--pool_name POOL_NAME", "Name of pool (will be created if it doesn't exist)") {|x| args[:pool_name] = x }
  opts.separator "Other optional arguments:"
  opts.on("-l", "--lb_method [LB_METHOD]", "Load balancing method (defaults to LB_METHOD_ROUND_ROBIN)") {|x| args[:pool_lb] = x }
  opts.on("-o", "--monitor [MONITOR]", "Set pool monitor") {|x| args[:pool_monitor] = x }
  opts.on("-m", "--members [SERVER1:PORT,SERVER2:PORT,...]", "Pool members, comma-separated") {|x| args[:pool_members] = x.split(",") }
end.parse!

# Beginning of script
lb = F5lib.new(args)

# create empty pool if it doesn't exist already
if lb.get_pool_status(args[:pool_name])
  puts "Using existing pool #{args[:pool_name]}"
else
  lb.new_pool(args[:pool_name], args[:pool_lb] ||= 'LB_METHOD_ROUND_ROBIN', args[:pool_members] ||= [])
  puts "Created new pool #{args[:pool_name]}"
end

if args[:pool_monitor]
  lb.set_pool_monitor(args[:pool_name], args[:pool_monitor])
  puts "Assigned pool monitor #{args[:pool_monitor]}"
end
