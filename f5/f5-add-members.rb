#!/opt/chef/embedded/bin/ruby

# F5 iControl for KP DevOps - Add one or more members to an existing pool
# Expects: Command-line arguments
# Returns:
#  Success: Exit code 0 with string of pool name
#  Success (already exists): Exit code 0, no string

# Aaron Zink - azink@trace3.com
# Version 1.00

require 'rubygems'
require 'optparse'
require './lib/f5-kp-library'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Add members to an existing pool\nUsage: f5-add-members.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required F5 arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Big-IP") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-n", "--pool_name POOL_NAME", "Name of pool") {|x| args[:pool_name] = x }
  opts.on("-m", "--members SERVER1:PORT,SERVER2:PORT,...", "Pool members, comma-separated") {|x| args[:pool_members] = x.split(",") }
end.parse!

# Beginning of script
lb = F5lib.new(args)

lb.add_pool_members(args[:pool_name], args[:pool_members])

puts "Success: Added #{args[:pool_members].join(", ")} to pool #{args[:pool_name]}'"
