#!/opt/chef/embedded/bin/ruby

# F5 iControl for KP DevOps - Assign pool to virtual servers, either specific VS or available VS
# Expects: Command-line arguments
# Returns:
#  Success: Exit code 0 with string of IP of virtual server
#  Error: Exit code 1 with error message

# Aaron Zink - azink@trace3.com
# Version 1.01

require 'rubygems'
require 'optparse'
require './lib/f5-kp-library'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Assign a default pool to an existing Virtual Server or new empty VS matching a regex\nUsage: f5-assign-pool.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required F5 arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Big-IP") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-n", "--pool_name POOL_NAME", "Name of pool") {|x| args[:pool_name] = x }
  opts.on("-r", "--allowed_vs_regex [REGEX]", "Allowed Virtual Server match regex, defaults to any") {|x| args[:allowed_vs_regex] = Regexp.new x }
end.parse!

# Beginning of script
lb = F5lib.new(args)

# Find first available virtual server that matches the allowed prefix and does not have a pool assigned, and assign pool
if vs = lb.find_vs_by_pool(args[:pool_name])
  puts lb.get_vs_ip(vs)
else
  vs = lb.get_available_vs(args[:allowed_vs_regex])
  if vs
    lb.set_vs_pool(vs, args[:pool_name])
    puts lb.get_vs_ip(vs)
  else
    abort "ERROR: No available virtual servers matching the allowed vip regex"
  end
end
