#!/opt/chef/embedded/bin/ruby

# F5 iControl for KP DevOps - Delete pool if pool already exists
# Expects: Command-line arguments
# Returns:
#  Success: Exit code 0 with string of pool name
#  Success (already exists): Exit code 0, no string

# Version 1.00

require 'rubygems'
require 'optparse'
require './lib/f5-kp-library'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Delete pool\nUsage: f5-delete-pool.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required F5 and pool arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Big-IP") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-n", "--pool_name POOL_NAME", "Name of pool to be deleted") {|x| args[:pool_name] = x }
end.parse!

# Beginning of script
lb = F5lib.new(args)

# delete pool if it exists
if lb.get_pool_status(args[:pool_name])
  lb.delete_pool(args[:pool_name])
  puts "Deleted new pool #{args[:pool_name]}"
else
  puts "The pool #{args[:pool_name]} does not exist"
end
