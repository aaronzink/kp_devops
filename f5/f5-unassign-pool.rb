#!/opt/chef/embedded/bin/ruby

# F5 iControl for KP DevOps - Unassign pool from virtual server
# Expects: Command-line arguments
# Returns:
#  Success: Exit code 0
#  Error: Exit code 1 with error message

# Version 1.01

require 'rubygems'
require 'optparse'
require './lib/f5-kp-library'

# Parse command-line options
args = {}
OptionParser.new do |opts|
  opts.banner = "Unassign the default pool from an existing Virtual Server\nUsage: f5-assign-pool.rb [options]"
  opts.on("-h", "--help", "Displays this help") { puts opts; exit }
  opts.separator "Required F5 arguments:"
  opts.on("-b", "--hostname HOSTNAME", "Hostname or IP of management interface of Big-IP") {|x| args[:hostname] = x }
  opts.on("-u", "--username USERNAME", "Username for Big-IP") {|x| args[:username] = x }
  opts.on("-p", "--password PASSWORD", "Password for Big-IP") {|x| args[:password] = x }
  opts.separator "Script-specific required arguments:"
  opts.on("-n", "--pool_name POOL_NAME", "Name of pool") {|x| args[:pool_name] = x }
end.parse!

# Beginning of script
lb = F5lib.new(args)

# Find first available virtual server that matches the allowed prefix and does not have a pool assigned, and assign pool
if vs = lb.find_vs_by_pool(args[:pool_name])
  lb.set_vs_pool(vs, nil)
  puts lb.get_vs_ip(vs)
else
  abort "ERROR: No virtual servers matching the pool name"
end
