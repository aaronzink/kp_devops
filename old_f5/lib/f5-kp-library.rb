#!/usr/bin/env ruby
# F5 iControl function library for devops
# Aaron Zink - azink@trace3.com
# Version 0.02
# 
# This library provides KP DevOps access to iControl using the f5-icontrol gem.
# It contains functions specific to DevOps and not the entire iControl library.
# See https://devcentral.f5.com/wiki/iControl.APIReference.ashx to add additional functions.

require 'f5/icontrol'

class F5lib
  def initialize(config)
    # Initialize connection and API
    F5::Icontrol.configure do |f|
      f.host = config[:hostname]
      f.username = config[:username]
      f.password = config[:password]
    end
    @bigip = F5::Icontrol::API.new
  end

  def get_pool_status(pool_name)
    # Get the status of the pool, error if it does not exist
    begin
      status = @bigip.LocalLB.Pool.get_object_status(pool_names: { item: pool_name})
    rescue Savon::SOAPFault
      # already exists, silently continue
      return false
    else
      return status[:item]
    end
  end

  def new_pool(pool_name, lb_method = 'LB_METHOD_ROUND_ROBIN', members = [])
    # Create new pool with zero or more members: new_pool(pool_name, lb_method, [member, member, ...])
    members.map! {|x| {:address => x.split(":")[0], :port => x.split(":")[1]} }
    @bigip.LocalLB.Pool.create(pool_names: { item: pool_name}, lb_methods: { item: lb_method }, members: {item: [members]})
  end

  def add_pool_members(pool_name, members)
    members.each do |member|
      member = [{:address => member.split(":")[0], :port => member.split(":")[1]}]
      begin
        @bigip.LocalLB.Pool.add_member_v2(pool_names: { item: pool_name}, members: {item: [member]})
      rescue Savon::SOAPFault => e
        puts e.message
      end
    end
  end

  def remove_pool_members(pool_name, members)
    members.each do |member|
      member = [{:address => member.split(":")[0], :port => member.split(":")[1]}]
      begin
        @bigip.LocalLB.Pool.remove_member_v2(pool_names: { item: pool_name}, members: {item: [member]})
      rescue Savon::SOAPFault => e
        puts e.message
      end
    end
  end


  def get_all_vss
    # Return an array of all virtual servers
    @bigip.LocalLB.VirtualServer.get_list[:item].map {|p| p.split("/").last}
  end

  def get_vs_pool(pool_name)
    # Return the name of the default pool, or nil
    @bigip.LocalLB.VirtualServer.get_default_pool_name(virtual_servers: {item: pool_name})[:item]
  end

  def get_available_vs(allowed = /.*/)
    # Find a virtual server with no default pool and matching allowed regex
    self.get_all_vss.keep_if {|v| v =~ allowed}.sort.each do |vs|
      return vs if self.get_vs_pool(vs).nil?
    end
    return nil
  end

  def find_vs_by_pool(pool_name)
    # check if pool is assigned to a VS and return the VS name, false if is unassigned
    all_vss = self.get_all_vss
    vs = @bigip.LocalLB.VirtualServer.get_default_pool_name(virtual_servers: {item: all_vss})[:item].zip(all_vss).select {|x| x[0] =~ /\/#{pool_name}$/}.flatten[1]
    return false if vs.nil?
    return vs.split("/").last
  end

  def set_vs_pool(vs, pool_name)
    @bigip.LocalLB.VirtualServer.set_default_pool_name(virtual_servers: {item: vs}, default_pools: {item: pool_name})
  end

  def get_vs_ip(vs)
    address_item = @bigip.LocalLB.VirtualServer.get_destination_v2(virtual_servers: {item: vs})
    address_item[:item][:address].split("/").last + ":" + address_item[:item][:port]
  end

  def set_pool_monitor(pool_name, monitor_name, monitor_type = 'MONITOR_RULE_TYPE_SINGLE')
    # Set pool monitor
    @bigip.LocalLB.Pool.set_monitor_association(monitor_associations: {item: {pool_name: pool_name, monitor_rule: {type: monitor_type, quorum: "0", monitor_templates: {item: monitor_name}}}})
  end

  # def get_all_pools
  #   # Return an array of all pool names
  #   @bigip.LocalLB.Pool.get_list[:item].to_s.to_a.map {|p| p.split("/").last}
  # end

end